# Getting Started with Angular Material

Here's a step-by-step guide to enabling Angular Material in your Angular CLI app.


### Dependencies

    ng new my-material-app
    cd my-material-app
    
    npm i -S @angular/material @angular/cdk @angular/animations
    npm i -S hammerjs
    

### main.ts

    import 'hammerjs';

`hammerjs` is needed for animation. (See below.)


### style.css

A material theme is required. If you use CLI, just put something like this in your `style.css`:

    @import "~@angular/material/prebuilt-themes/indigo-pink.css";

`@Angular/Material` currently supports four built-in themes: _deeppurple-amber, indigo-pink, pink-bluegrey,_ and _purple-green._


### .angular-cli.json

If you want to use a custom theme, create a theme scss file, and add it under the `styles` section in `angular-cli.json` (if you use CLI).

    "apps": [{ "styles": [ "my-custom-theme.scss", "styles.css"], ... } ]


Here's an example custom theme file:

* [my-custom-theme.scss](samples/my-custom-theme.scss)



### index.html

Add the link to the Material icons:

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

Then, add the `mat-app-background` style/attribute to a top-level element such as `<body>` or `<app-root>`. 
(Do this only if your app is not contained in `<mat-sidenav-container>`.)


### app.module.ts

(Optional) If you use a custom theme with alternate/secondary themes, 
then you need to enable the style classes in the app module in order to be able to use them in overlay components (e.g., menus, dialogs):

    import { OverlayContainer } from '@angular/cdk/overlay';

    export class AppModule {
      constructor(private overlayContainer: OverlayContainer) {
        overlayContainer.getContainerElement().classList.add('my-alternate-custom-theme');
      }
    }

where `my-alternate-custom-theme` is the name of an alternate theme style selector.


### Angular Material component module

If you use Angular Material, then you'll likely end up using quite a few Angular Material components (but not all).
For organizational purposes,
create an extra module to import/export all material components/modules used in the app, and import it into `AppModule`:

    import { MyMaterialComponentsModule } from './my-material-components.module';

    @NgModule({
      ...
      imports: [
        BrowserModule,
        MyMaterialComponentsModule
      ],
      ...
    })
    export class AppModule { }



Here's an example material components module:

* [MyMaterialComponentsModule](samples/my-material-components.module.ts)


Note that you can use `NoopAnimationsModule` instead of `BrowserAnimationsModule` if you don't need animation.



## References

* [Angular Material](https://material.angular.io/)
* [Getting Started](https://material.angular.io/guide/getting-started)
* [Components](https://material.angular.io/components/categories)
* [angular/material2](https://github.com/angular/material2)
* [Theming your Angular Material app](https://material.angular.io/guide/theming)
* [Material Style - Color](https://material.io/guidelines/style/)
* [Material Design - Icons](https://material.io/icons/)
* [angular/flex-layout](https://github.com/angular/flex-layout)
* []()
* []()

